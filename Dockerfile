FROM phalconphp/php-fpm:7

RUN apt-get update && apt-get install -y git acl

RUN curl -sS https://getcomposer.org/installer | php && \
    mv composer.phar /usr/local/bin/composer

ADD BB-Tracker /data/vhost/BB-Tracker
ADD bin /data/bin

RUN cd /data/vhost/BB-Tracker && \
    composer install --no-dev && \
    chown -R www-data:www-data /data/vhost && \
    setfacl -R -m u:www-data:rwx /data/vhost/ && \
    setfacl -R -d -m u:www-data:rwx /data/vhost/

VOLUME /data/vhost/BB-Tracker
WORKDIR /data/vhost/BB-Tracker
ENTRYPOINT ["php-fpm7.0", "-F"]