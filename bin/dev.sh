#! /bin/bash
cd /data/vhost/BB-Tracker
composer install
ln -s /data/vhost/BB-Tracker/vendor/bin/phalcon.php /usr/bin/phalcon
chmod ugo+x /usr/bin/phalcon
sudo -u www-data phalcon webtools enable
#Start
php-fpm7.0 -F