CREATE TABLE `userauth_user` (
    `id` int (10) unsigned NOT NULL AUTO_INCREMENT,
    `accountType` varchar (45) NOT NULL ,
    `authId` varchar (255) NOT NULL,
    `firstName` varchar(45) NULL,
    `lastName` varchar(45) NULL,
    `email` varchar(45) NOT NULL,
    `avatar` varchar(255) NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `bbtracker_projects` (
    `id` int (10) unsigned NOT NULL AUTO_INCREMENT,
    `title` varchar (45) NOT NULL ,
    `description` varchar (255),
    PRIMARY KEY (`id`)
);

CREATE TABLE `bbtracker_tasks` (
    `id` int (10) unsigned NOT NULL AUTO_INCREMENT,
    `title` varchar (45) NOT NULL ,
    `description` varchar (255),
    `type` varchar (45) NOT NULL,
    `categoryId` int (10) unsigned NOT NULL,
    `createdAt` int (10) unsigned NOT NULL,
    `reporterId` int (10) unsigned NOT NULL,
    `severity` int (10) unsigned NOT NULL,
    `projectId` int (10) unsigned NOT NULL,
    `closed` int (1) unsigned NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`)
);

CREATE TABLE `bbtracker_tasks_version` (
    `taskId` int (10) unsigned NOT NULL,
    `version` int (10) unsigned NOT NULL,
    `assigneeId` int (10) unsigned NOT NULL,
    `status` varchar (255) NOT NULL,
    `updatedAt` int (10) unsigned NOT NULL,
    `updaterId` int (10) unsigned NOT NULL,
    `changes` varchar (255),
    PRIMARY KEY (`taskId`, `version`)
);