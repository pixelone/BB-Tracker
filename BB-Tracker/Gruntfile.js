'use strict';
module.exports = function (grunt) {
    grunt.initConfig({
        compass: {
            dist: {
                options: {
                    sassDir: ['public/sass'],
                    cssDir: ['public/css'],
                    environment: 'production'
                }
            },
            dev: {
                options: {
                    sassDir: ['public/sass'],
                    cssDir: ['public/css'],
                    environment: 'development',
                    outputStyle: 'expanded'
                }
            }
        },
        watch: {
            compass: {
                tasks: ['compass:dev'],
                files: ['public/sass/**']
            }
        },
        concurrent: {
            pre: ['compass:dev'],
            post: []
        }
    });
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('build', ['compass:dist']);
    return grunt.registerTask('default', ['build']);
};
