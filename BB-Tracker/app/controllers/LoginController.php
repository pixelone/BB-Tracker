<?php
use \Phalcon\Mvc\Controller;
Use \UserAuth\Handler\GoogleAuthHandler;

class LoginController extends JsonController
{

    public function indexAction()
    {
        $client = new Google_Client();
        $client->setAuthConfig(GoogleAuthHandler::JSONFILE);
        $client->setRedirectUri(GoogleAuthHandler::createRedirectUri());
        $client->setScopes(['profile', 'email']);
        $this->view->googleAuthUrl = $client->createAuthUrl();
    }

    public function googleAuthAction()
    {
        if($this->request->has('code')){
            $clientToken = $this->request->get('code');
            try{
                $this->userAuth->createSession('GoogleAuthHandler', $clientToken);
                $this->response->redirect();
                $this->view->disable();
            } catch (\Exception $e){
                $this->flash->error("Error while trying to connect: ". $e->getMessage());
            }
        } else {
            $this->response->redirect('/');
        }
    }

    public function logoutAction(){
        //Logout user
        if($this->userAuth->destroySession()){
            $this->response->redirect();
            $this->view->disable();
        } else {
            $this->flash->error("Error while trying to logOut");
        };
    }

}

