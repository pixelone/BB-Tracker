<?php

class TaskController extends JsonController
{

    public function viewAction($id = null){
        $this->view->task = TaskRepository::findById($id);
    }
    public function get($id = null){
        if(!is_numeric($id)){
            return TaskRepository::findAll();
        } else {
            return TaskRepository::findById($id);
        }
    }

    public function post($id = null){
        $formData = (object) $this->request->getJsonRawBody(true);
        $newTask = TaskRepository::createNew(
            $formData->title,
            $formData->description,
            $formData->type,
            $formData->categoryId,
            1,
            $formData->severity,
            $formData->projectId,
            $formData->assigneeId
        );
        return $newTask;
    }

    public function put($id){
        $Task = TaskRepository::findById($id);
        if($Task instanceof TaskRepository){
            $formData = (object) $this->request->getJsonRawBody(true);
            $Task->updateTask(
                $formData->title,
                $formData->description,
                $formData->type,
                $formData->categoryId,
                1,
                $formData->severity,
                $formData->assigneeId
            );
            return $Task;
        } else {
            return false;
        }
    }

    public function delete($id){
        $Task = TaskRepository::findById($id);
        if($Task instanceof TaskRepository){
            $Task->closeTask();
            return true;
        } else {
            return false;
        }
    }
}

