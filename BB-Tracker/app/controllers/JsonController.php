<?php
/**
 *
 * php5 JsonController.php
 *
 * @copyright   Copyright (C) 2017 Moonda. All rights reserved.
 * @author      Fabien Robert (fro@moonda.com)
 */

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;

abstract class JsonController extends Controller
{
    public function jsonAction($param = null){
        $this->view->disable();
        $response = new Response();
        $method = strtolower($this->request->getMethod());
        $content = array(
            'data' => false,
            'error' => false
        );
        if(method_exists($this, $method)){
            try{
                $content['data'] = $this->$method($param);
            } catch(Exception $e){
                $content['error'] = true;
                $content['errorMessage'] = $e->getMessage();
            }
        };
        $response->setJsonContent($content);
        return $response;
    }

}