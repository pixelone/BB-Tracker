<?php

class ProjectController extends JsonController
{

    public function indexAction($id)
    {
        $this->view->taskList = TaskRepository::findAllByProjectId($id);
    }

    public function get($id = null){
        if(is_numeric($id)){
            return Projects::findFirst($id);
        } else {
            return Projects::find()->toArray();
        }
    }

    public function post($id = null){
        $project = new Projects();
        $formData = $this->request->getJsonRawBody(true);
        foreach($formData as $key => $item){
            $project->$key = $item;
        }
        $project->create();
        return $project;
    }

    public function put($id){
        $project = Projects::findFirst($id);
        if($project instanceof Projects){
            $formData = $this->request->getJsonRawBody(true);
            foreach($formData as $key => $item){
                $project->$key = $item;
            }
            $project->save();
            return true;
        } else {
            return false;
        }
    }

    public function delete($id){
        $project = Projects::findFirst($id);
        if($project instanceof Projects){
            $project->delete();
            return true;
        } else {
            return false;
        }
    }
}

