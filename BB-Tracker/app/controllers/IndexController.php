<?php
use Phalcon\Mvc\Controller;

class IndexController extends Controller
{

    public function indexAction()
    {
        if($this->userAuth->userIsLoggedIn){
            $this->response->redirect('index/dashboard');
        } else {
            $this->view->userTarget = (object) array(
                'url' => $this->url->get("login"),
                'title' => 'Login'
            );
            $client = new Google_Client();
            $client->setAuthConfig(\UserAuth\Handler\GoogleAuthHandler::JSONFILE);
            $client->setRedirectUri(\UserAuth\Handler\GoogleAuthHandler::createRedirectUri());
            $client->setScopes(['profile', 'email']);
            $this->view->googleAuthUrl = $client->createAuthUrl();
        }
    }

    public function dashboardAction(){
        if(!$this->userAuth->userIsLoggedIn){
            $this->response->redirect('/');
        } else {
            $this->view->projects = Projects::find();
        }
    }
}

