<?php
use \Phalcon\Mvc\Controller;
use UserAuth\Entity\Users;

class UserController extends Controller
{

    public function indexAction()
    {
        $this->view->disable();
        $this->response->redirect('user/list');
    }

    public function listAction()
    {
        $this->view->disable();
        $this->response->setContentType('application/json', 'UTF-8');
        $users = Users::find();
        $this->response->setContent(json_encode($users));
        return $this->response;
    }
}

