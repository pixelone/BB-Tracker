<?php
/**
 *
 * php5 TasksHistory.php
 *
 * @copyright   Copyright (C) 2017 Moonda. All rights reserved.
 * @author      Fabien Robert (fro@moonda.com)
 */

use Phalcon\Mvc\Application\Exception;
use Phalcon\Mvc\Model;

class TasksVersion extends Model {

    /**
     * @var int
     */
    protected $taskId;
    /**
     * @var int
     */
    protected $version;
    /**
     * @var int
     */
    protected $assigneeId;
    /**
     * @var string
     */
    protected $status;
    /**
     * @var int
     */
    protected $updatedAt;
    /**
     * @var int
     */
    protected $updaterId;

    /**
     * @var string
     */
    protected $changes;


    public function initialize(){
        $this->setSource("bbtracker_tasks_version");
        $this->belongsTo(
            "taskId",
            "Tasks",
            "id"
        );
    }

    /**
     * @param Tasks $task
     * @param int $version
     * @param int $assigneeId
     * @param string $status
     * @param $updaterId
     * @param $changes
     * @return static
     */
    public static function createVersion(Tasks $task, int $version, int $assigneeId, $status = '', $updaterId, $changes = null){
        $Version = new static();
        $Version->setTaskId($task->getId());
        $Version->setVersion($version);
        $Version->setAssigneeId($assigneeId);
        $Version->setStatus($status);
        $Version->setUpdaterId($updaterId);
        $Version->setChanges($changes);
        $Version->setUpdatedAt();
        return $Version;
    }

    public static function sortVersion(TasksVersion $versionA,TasksVersion $versionB){
        if($versionA->taskId !== $versionB->taskId) throw new Exception('Trying to sort versions of different bugs');
        if($versionA->version == $versionB->version) throw new Exception('Cannot have to same version for a same Task');
        return ($versionA->version > $versionB->version) ? +1 : -1;
    }


    /**
     * @return int
     */
    public function getTaskId(){
        return $this->taskId;
    }

    /**
     * @return int
     */
    public function setTaskId(int $taksId){
        $this->taskId = $taksId;
        return $this->taskId;
    }

    /**
     * @return int
     */
    public function getVersion(){
        return $this->version;
    }

    /**
     * @return int
     */
    public function setVersion(int $version){
        $this->version = $version;
        return $this->version;
    }

    /**
     * @return int
     */
    public function getAssigneeId(){
        return $this->assigneeId;
    }

    /**
     * @param int $assigneeId
     * @throws Exception
     * @return int
     */
    public function setAssigneeId(int $assigneeId){
        if(\UserAuth\Entity\Users::count(array("id='$assigneeId'")) == 0) throw new Exception('Assignee Doesn\'t exists');
        $this->assigneeId = $assigneeId;
        return $this->assigneeId;
    }

    /**
     * @return string
     */
    public function getStatus(){
        return $this->status;
    }

    /**
     * @param string $status
     * @return string
     */
    public function setStatus(string $status){
        $this->status = $status;
        return $this->status;
    }

    /**
     * @return int
     */
    public function getUpdatedAt(){
        return $this->updatedAt;
    }

    /**
     * @return int
     */
    public function setUpdatedAt(){
        $this->updatedAt = time();
        return $this->updatedAt;
    }

    /**
     * @return int
     */
    public function getUpdaterId(): int
    {
        return $this->updaterId;
    }

    /**
     * @param int $updaterId
     */
    public function setUpdaterId(int $updaterId){
        $this->updaterId = $updaterId;
    }

    /**
     * @return string
     */
    public function getChanges(): string
    {
        return $this->changes;
    }

    /**
     * @param string $changes
     */
    public function setChanges($changes = '')
    {
        $this->changes = $changes;
    }


}
