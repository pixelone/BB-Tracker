<?php
/**
 *
 * php5 User.php
 *
 * @copyright   Copyright (C) Fabien Robert. All rights reserved.
 * @author      Fabien Robert (fabien@pixelone.fr)
 */

use Phalcon\Mvc\Model;

class Projects extends Model
{
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $title;
    /**
     * @var string|null
     */
    protected $description;

    public function initialize()
    {
        $this->setSource("bbtracker_projects");
    }

    /**
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return void
     */
    public function setId(){
        return;
    }

    /**
     * @return string
     */
    public function getTitle(){
        return $this->title;
    }

    /**
     * @param $title
     * @return string
     */
    public function setTitle($title){
        $this->title = $title;
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(){
        return $this->title;
    }

    /**
     * @param $title
     * @return string
     */
    public function setDescription($title){
        $this->description = $title;
        return $this->description;
    }
}