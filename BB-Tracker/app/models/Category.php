<?php
/**
 *
 * php5 Category.php
 *
 * @copyright   Copyright (C) 2017 Moonda. All rights reserved.
 * @author      Fabien Robert (fro@moonda.com)
 */

use Phalcon\Mvc\Model;

class Category extends Model
{
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $name;
    /**
     * @var string
     */
    protected $description;

    public function initialize(){

    }

    /**
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return void
     */
    public function setId(){
        return;
    }

    /**
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * @param string $name
     * @return string
     */
    public function setName(string $name){
        $this->name = $name;
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(){
        return $this->description;

    }

    /**
     * @param string $description
     * @return string
     */
    public function setDescription(string $description){
        $this->description = $description;
        return $this->description;
    }


}