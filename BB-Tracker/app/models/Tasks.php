<?php
/**
 *
 * php5 User.php
 *
 * @copyright   Copyright (C) Fabien Robert. All rights reserved.
 * @author      Fabien Robert (fabien@pixelone.fr)
 */

use Phalcon\Mvc\Model;

class Tasks extends Model
{

    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $title;
    /**
     * @var string
     */
    protected $description;
    /**
     * @var string
     */
    protected $type;
    /**
     * @var int
     */
    protected $categoryId;
    /**
     * Timestamp
     * @var int
     */
    protected $createdAt;
    /**
     * User ID
     * @var int
     */
    protected $reporterId;
    /**
     * @var int
     */
    protected $severity;
    /**
     * Projects->id
     * @var int
     */
    protected $projectId;

    /**
     * @var int
     */
    protected $closed = 0;

    /**
     * @var Projects
     */
    //public $Projects;

    /**
     * @var TasksVersion
     */
    //public $TasksVersion;

    public function initialize(){
        $this->setSource("bbtracker_tasks");
        $this->hasOne('reporterId', "\\UserAuth\\Entity\\Users", 'id', ['alias' => 'reporter']);
        $this->hasOne("projectId", "Projects", "id");
        $this->hasMany("id", "TasksVersion", "taskId");
    }

    public static function createNew($title, $description, $type, $categoryId, $reporterId, $severity, $projectId){
        $task = new static();
        $task->setTitle($title);
        $task->setDescription($description);
        $task->setType($type);
        $task->setCategoryId($categoryId);
        $task->setCreatedAt();
        $task->setReporterId($reporterId);
        $task->setSeverity($severity);
        $task->setProjectId($projectId);
        $task->create();
        return $task;
    }

    /**
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return void
     */
    public function setId(){
        return;
    }

    /**
     * @return string
     */
    public function getTitle(){
        return $this->title;
    }

    /**
     * @param string $title
     * @return string
     */
    public function setTitle(string $title){
        $this->title = $title;
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(){
        return $this->description;
    }

    /**
     * @param string $description
     * @return string
     */
    public function setDescription(string $description){
        $this->description = $description;
        return $this->description;
    }

    /**
     * @return string
     */
    public function getType(){
        return $this->type;
    }

    /**
     * @param string $type
     * @return string
     */
    public function setType(string $type){
        $this->type = $type;
        return $this->type;
    }

    /**
     * @return int
     */
    public function getCategoryId(){
        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     * @throws Exception
     * @return int
     */
    public function setCategoryId($categoryId){
        if(!is_numeric($categoryId)) throw new Exception('A Task must have a valid Category ID');
        $this->categoryId = $categoryId;
        return $this->categoryId;
    }

    /**
     * @return int
     */
    public function getCreatedAt(){
        return $this->createdAt;
    }

    /**
     * @return int
     */
    public function setCreatedAt(){
        $this->createdAt = time();
        return $this->createdAt;
    }

    /**
     * @return int
     */
    public function getReportedId(){
        return $this->reporterId;
    }

    /**
     * @param $reportedId
     * @return int
     */
    public function setReporterId($reportedId){
        $this->reporterId = $reportedId;
        return $this->reporterId;
    }

    /**
     * @return int
     */
    public function getSeverity(){
        return $this->severity;
    }

    /**
     * @param int $severity
     * @return int
     */
    public function setSeverity($severity){
        if(!is_numeric($severity)) $severity = 5;
        $this->severity = $severity;
        return $this->severity;
    }

    /**
     * @return int
     */
    public function getProjectId(){
        return $this->projectId;
    }

    /**
     * @param int $projectId
     * @return int
     */
    public function setProjectId(int $projectId){
        $this->projectId = $projectId;
        return $this->projectId;
    }

    /**
     * @return bool
     */
    public function getClosed(): bool
    {
        return ($this->closed == 1) ? true : false;
    }

    /**
     * @param bool $closed
     */
    public function setClosed(bool $closed)
    {
        $this->closed = ($closed) ? 1 : 0;
    }

}