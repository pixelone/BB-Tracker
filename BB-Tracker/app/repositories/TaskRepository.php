<?php
/**
 *
 * php5 Tasks.php
 *
 * @copyright   Copyright (C) 2017 Moonda. All rights reserved.
 * @author      Fabien Robert (fro@moonda.com)
 */

class TaskRepository
{
    /**
     * @var Tasks
     */
    public $task;
    /**
     * @var TasksVersion[]
     */
    public $history = array();

    /**
     * @var int
     */
    public $id;
    /**
     * @var int
     */
    public $version = 0;

    public static function findById(int $TaskId){
        $Task = new static();
        $Task->task = Tasks::findFirst($TaskId);
        if(!$Task->task instanceof Tasks) throw new Exception('Could\'t find task');
        $Task->fetchVersion();
        return $Task;
    }

    /**
     * @return static[]
     */
    public static function findAll()
    {
        $taskArray = array();
        /** @var Tasks[] $taskList */
        $taskList = Tasks::find();
        foreach ($taskList as $task) {
            $taskItem = new static();
            $taskItem->task = $task;
            $taskItem->fetchVersion();
            $taskArray[] = $taskItem;
        };
        return $taskArray;
    }

    public static function findAllByProjectId(int $ProjectId){
        $taskArray = array();
        /** @var Tasks[] $taskList */
        $taskList = Tasks::findByProjectId($ProjectId);
        foreach ($taskList as $task) {
            $taskItem = new static();
            $taskItem->task = $task;
            $taskItem->fetchVersion();
            $taskArray[] = $taskItem;
        };
        return $taskArray;
    }

    /**
     * @param $title
     * @param $description
     * @param $type
     * @param int $categoryId
     * @param $reporterId
     * @param int $severity
     * @param $projectId
     * @param $assigneeId
     * @return static
     */
    public static function createNew($title, $description, $type, $categoryId = 1, $reporterId, $severity = 5, $projectId, $assigneeId = 1){
        $NewTask = new static();
        $NewTask->task = Tasks::createNew($title, $description, $type, $categoryId, $reporterId, $severity, $projectId);
        $NewTask->addVersion($assigneeId, 'new');
        $NewTask->assignData();
        return $NewTask;
    }

    public function fetchVersion(){
        $this->history = array();
        foreach($this->task->TasksVersion as $history){
            /** @var $history TasksVersion */
            $this->history[] = $history;
        };
        $this->assignData();
    }

    public function closeTask(){
        if(!$this->task->getClosed()){
            $this->addVersion(0, 'closed');
            $this->task->setClosed(true);
            $this->task->save();
        }
    }

    public function updateTask($title = null, $description = null, $type = null, $categoryId = null, $updaterId = null, $severity = null, $assigneeId = null){
        $changes = array();
        if($title) $changes['title'] = $this->task->setTitle($title);
        if($description) $changes['description'] = $this->task->setDescription($description);
        if($type) $changes['type'] = $this->task->setType($type);
        if($categoryId) $changes['categoryId'] = $this->task->setCategoryId($categoryId);
        if($severity) $changes['updaterId'] = $this->task->setSeverity($severity);
        $newVersion = TasksVersion::createVersion($this->task, $this->version + 1, ($assigneeId) ? $assigneeId : $this->history[$this->version]->getAssigneeId(), 'updated', $updaterId);

        if (in_array(false, $changes)){
            //TODO Add list of failed attribute
            throw new Exception('Failed to update Task '.$this->id);
        } else {
            //TODO Store list of changes in version
            $this->task->save();
            $newVersion->setChanges(implode(',', array_keys($changes)));
            $newVersion->save();
            $this->history[$newVersion->getVersion()] = $newVersion;
            $this->version++;
        }
    }

    public function assignData(){
        if(!$this->task instanceof Tasks) throw new \Exception('No Task found');
        usort($this->history, 'TasksVersion::sortVersion');
        $this->id = $this->task->getId();
        $this->version = (count($this->history) > 0) ? end($this->history)->getVersion() : 0;
    }

    public function addVersion(int $newAssignee = 0, $status = 'new'){
        $initialVersion = TasksVersion::createVersion($this->task, ++$this->version, $newAssignee, $status, $this->task->getReportedId(), 'created');
        $initialVersion->create();
        $this->history[] = $initialVersion;

    }

}