{% if userAuth.userIsLoggedIn %}
    <nav class="top-navbar">
        <div class="nav-wrapper">
            <div class="right profile-container">
                    <a class='dropdown-button btn-flat' href='#' data-activates='userDropdown'>
                        Logged as {{ userAuth.userLoggedIn.firstName }}
                    </a>
                    <ul id='userDropdown' class='dropdown-content'>
                        <li><a href="{{ url.get('user') }}">View profile</a></li>
                        <li><a href="{{ url.get('login/logout') }}">logout</a></li>
                    </ul>
                    <img src="{{ userAuth.userLoggedIn.avatar }}" class="avatar circle right">
            </div>
            <ul id="nav-mobile" class="left hide-on-med-and-down">
                <li><a href="{{ url.get('index/dashboard') }}">Index</a></li>
                <li><a href="{{ url.get('user') }}">Users</a></li>
            </ul>
        </div>
    </nav>
{% endif %}
