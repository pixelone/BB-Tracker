<div class="container">
        <div class="collection">
            {% for task in taskList %}
                    <a href="{{ url('task/view/' ~ task.task.id) }}" class="collection-item">#{{ task.task.id }} - {{task.task.title}}
                        {% if task.task.closed %}<span class="badge">closed</span>{% endif %}
                    </a>
            {% endfor %}
        </div>
</div>