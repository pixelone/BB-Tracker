<div class="container">
    <div class="valign-wrapper">
        <h1>Dashboard</h1>
        <div class="valign right">
            <a class="btn-floating btn-medium waves-effect waves-light red right tooltipped" data-position="bottom" data-delay="0" data-tooltip="Create a project"><i class="material-icons">add</i></a>
        </div>
    </div>
    {% if projects|length > 0 %}
        <h5>Projects</h5>
        <div class="collection">
            {% for project in projects %}
                <a href="{{ url.get('project/index/' ~ project.id) }}" class="collection-item"><span class="new badge">1</span>{{ project.title }}</a>
            {% endfor %}
        </div>
    {% else %}
        <h4>There's no projects at the moment</h4>
    {% endif %}
</div>