<div class="background">
    <div class="mask"></div>
</div>
<div class="index container-fluid valign-wrapper">
    {% if not userAuth.userIsLoggedIn %}
        <div class="center-align valign full-width">
            <h1>BBugTracker </h1>
            <a class="waves-effect waves-light btn" href="#loginmodal">{{ userTarget.title }}</a>
        </div>
    {% endif %}
    <!-- Modal Structure -->
    <div id="loginmodal" class="modal">
        <div class="modal-content">
            <a href="{{ googleAuthUrl }}" class="modal-close waves-effect btn-flat">Login with Google</a>
        </div>
    </div>
</div>