<div class="row">
    <div class="valign-wrapper">
        <a class="waves-effect waves-light btn" href="{{ googleAuthUrl }}">{{ userTarget.title }}</a>
    </div>
</div>
<a class="waves-effect waves-light btn" href="#loginModal">Login</a>
<script>
    $(document).ready(function(){
        $('.modal').modal();
    });
</script>
