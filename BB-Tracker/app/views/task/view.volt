<div class="container">
    <div class="row">
        <div class="col s12 m6">
            <div class="card blue-grey darken-1">
                <div class="card-content white-text">
                    <span class="card-title">{{ task.task.title }}</span>
                    <p>{{ task.task.description }}</p>
                    <div class="chip">
                        User {{ task.history[0].assigneeId }}
                    </div>
                </div>
                <div class="card-action">
                    <a href="#">Move Backward</a>
                    <a href="#">Move Forward</a>
                </div>
            </div>
        </div>
    </div>
</div>