<?php
/**
 *
 * php5 AuthHandler.php
 *
 * @copyright   Copyright (C) 2017 Fabien Robert. All rights reserved.
 * @author      Fabien Robert (fabien@pixelone.fr)
 */

namespace UserAuth\Handler;

use Google_Client;
use Phalcon\Mvc\Dispatcher\Exception as PhalconException;
use UserAuth\Entity\Users;


class GoogleAuthHandler implements AuthHandlerInterface
{
    const JSONFILE = APP_PATH. '/config/oauth.json';
    const REDIRECT_URI = '/login/googleAuth';

    /** @var Google_Client */
    public $googleClient;

    /** @var array */
    public $clientToken;

    /** @var array */
    public $accessToken;

    /** @var array */
    public $jsonWebToken;

    public function __construct(){
        $this->googleClient = new Google_Client();
        $this->googleClient->setAuthConfig(self::JSONFILE);
        $this->googleClient->setRedirectUri(self::createRedirectUri());
    }

    public function resumeSession($sessionData){
        $this->accessToken = $sessionData['token'];
        $this->googleClient->setAccessToken($this->accessToken);
        $this->jsonWebToken = $this->googleClient->verifyIdToken();
        return Users::findByAuthId($this->jsonWebToken['sub']);
    }

    /**
     * @param mixed $clientToken
     * @return Users
     * @throws PhalconException
     */
    public function logIn($clientToken){
        $this->clientToken = $clientToken;
        $this->accessToken = $this->googleClient->fetchAccessTokenWithAuthCode($this->clientToken);
        if(isset($this->accessToken['error'])){
            throw new PhalconException($this->accessToken['error_description']);
        } else {
            $this->jsonWebToken = $this->googleClient->verifyIdToken();
            $requestedUser = Users::findByAuthId($this->jsonWebToken['sub']);
            if($requestedUser){
                //Existing user
                return $requestedUser;
            } else {
                //New user
                return Users::createNewUser(
                    'google',
                    $this->jsonWebToken['sub'],
                    $this->jsonWebToken['given_name'],
                    $this->jsonWebToken['family_name'],
                    $this->jsonWebToken['email'],
                    $this->jsonWebToken['picture']
                );
            }
        }
    }

    /**
     * @return boolean
     */
    public function logOut(){
        return true;
    }

    /**
     * @return array Data to store in the Phalcon Session service
     */
    public function sessionData(){
        return array(
            'authType' => __CLASS__,
            'data' => [
                'token' => $this->accessToken
            ]
        );
    }

    static function createRedirectUri(){
        $serverProtocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
        return $serverProtocol . $_SERVER['HTTP_HOST'] . self::REDIRECT_URI;
    }

}