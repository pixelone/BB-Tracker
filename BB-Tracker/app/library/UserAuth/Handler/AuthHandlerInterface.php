<?php
/**
 *
 * php5 AuthHandler.php
 *
 * @copyright   Copyright (C) 2017 Fabien Robert. All rights reserved.
 * @author      Fabien Robert (fabien@pixelone.fr)
 */

namespace UserAuth\Handler;

interface AuthHandlerInterface
{
    public function __construct();

    /**
     * inject session data to access user's information
     * @param mixed $sessionData Mixed data that the AuthHandler uses to get user's session
     * @return bool
     */
    public function resumeSession($sessionData);

    /**
     * @param mixed $loginData Mixed data that the AuthHandler uses to log user in
     * @return bool
     */
    public function logIn($loginData);

    /**
     * Expire session data
     * @return bool
     */
    public function logOut();

    /**
     * @return array Data to store in the Phalcon Session service
     */
    public function sessionData();
}