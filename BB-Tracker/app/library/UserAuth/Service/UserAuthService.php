<?php
/**
 *
 * php5 UserAuthService.php
 * Handler for the user authentication
 * Service is initialized at request and check for session variable
 *
 * @copyright   Copyright (C) 2017 Fabien Robert. All rights reserved.
 * @author      Fabien Robert (fabien@pixelone.fr)
 */

namespace UserAuth\Service;

use Phalcon\Session\Adapter\Files as Session;
use Phalcon\Mvc\Dispatcher\Exception;
use UserAuth\Entity\Users;


class UserAuthService
{
    /**
     * @var bool
     */
    public $userIsLoggedIn = false;
    /**
     * @var mixed
     */
    public $authHandler;

    /**
     * @var Users|null
     */
    public $userLoggedIn = null;

    /**
     * UserAuthService constructor.
     * @throws Exception
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
        if($this->session->has('UserAuth')){
            $this->userIsLoggedIn = $this->resumeSession($this->session->get('UserAuth'));
        } else {
            $this->userIsLoggedIn = false;
        }
        return $this;
    }

    /**
     * @param array $sessionData
     * @return bool True if the session has sucessfully resumed
     * @throws Exception
     */
    public function resumeSession(array $sessionData){
        try{
            if(isset($sessionData['authType'])){
                $authHandler = $sessionData['authType'];
                if($authHandler !== false){
                    /** @var $this->authHandler BBTracker\Handler\Auth\GoogleAuthHandler */
                    $this->authHandler = new $authHandler();
                    $this->userLoggedIn = $this->authHandler->resumeSession($sessionData['data']);
                    return ($this->userLoggedIn instanceof Users);
                } else {
                    throw new Exception('Fail to log user with session of type '. $sessionData['authType']);
                }
            } else {
                return false;
            }
        } catch (\Exception $e){
            return false;
        }
    }

    public function createSession(string $sessionType, $sessionData){
        $authHandler = self::authHandlerClass($sessionType);
        if($authHandler !== false){
            $this->authHandler = new $authHandler();
            $this->userLoggedIn = $this->authHandler->logIn($sessionData);
            if($this->userLoggedIn instanceof Users){
                $this->userIsLoggedIn = true;
                $this->session->set('UserAuth', $this->authHandler->sessionData());
            }
        } else {
            throw new Exception('Failed to instancied a authHandler Class');
        }
        return $this->userIsLoggedIn;
    }

    public function destroySession(){
        $this->session->remove('UserAuth');
        return true;
    }

    /**
     * @param string $authhandler
     * @return string
     */
    private function authHandlerClass(string $authhandler){
        $authHandlerClassPath = 'UserAuth\\Handler\\'.$authhandler;
        if(class_exists($authHandlerClassPath)){
            return $authHandlerClassPath;
        } else {
            return false;
        }
    }
}