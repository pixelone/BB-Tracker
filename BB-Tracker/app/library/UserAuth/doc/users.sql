CREATE TABLE `userauth_user` (
    `id` int (10) unsigned NOT NULL AUTO_INCREMENT,
    `accountType` varchar (45) NOT NULL ,
    `authId` varchar (255) NOT NULL,
    `firstName` varchar(45) NULL,
    `lastName` varchar(45) NULL,
    `email` varchar(45) NOT NULL,
    `avatar` varchar(255) NULL,
    PRIMARY KEY (`id`)
);