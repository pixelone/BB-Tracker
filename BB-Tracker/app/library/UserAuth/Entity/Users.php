<?php
/**
 *
 * php5 User.php
 *
 * @copyright   Copyright (C) 2017 Fabien Robert. All rights reserved.
 * @author      Fabien Robert (fabien@pixelone.fr)
 */

namespace UserAuth\Entity;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Dispatcher\Exception;

class Users extends Model
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    public $accountType;
    /**
     * @var string
     */
    public $authId;
    /**
     * @var string
     */
    public $firstName;
    /**
     * @var string
     */
    public $lastName;
    /**
     * @var string
     */
    protected $email;
    /**
     * @var string
     */
    public $avatar;

    public function initialize()
    {
        $this->setSource('userauth_user');
    }

    /**
     * @param $authId
     * @return Users|false
     */
    public static function findByAuthId($authId){
        return self::findFirst("authId='$authId'");
    }

    /**
     * @param string $accountType
     * @param string $authid
     * @param string $firstName
     * @param string $lastName
     * @param string $email
     * @param string $avatar
     * @return Users
     * @throws Exception
     */
    public static function createNewUser(string $accountType, string $authid, string $firstName, string $lastName, string $email, string $avatar){
        try{
            $newUser = new static();
            $newUser->setEmail($email);
            $newUser->accountType = $accountType;
            $newUser->authId = $authid;
            $newUser->firstName = $firstName;
            $newUser->lastName = $lastName;
            $newUser->avatar = $avatar;
            if ($newUser->save()) {
                return $newUser;
            } else {
                throw new Exception(implode("\n", $newUser->getMessages()));
            }
        } catch (\Exception $e){
            throw new Exception('Failed to create user: '.$e->getMessage());
        }
    }

    /**
     * @return string
     */
    public function getEmail(){
        return $this->email;
    }

    /**
     * @param string $email
     * @throws Exception
     * @return true
     */
    public function setEmail(string $email){
        if(trim($email) == '')throw new Exception('Email cannot be empty');
        if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->email = $email;
            return true;
        } else {
            throw new Exception('Invalid email provided: '.$email);
        }
    }

    public function getId(){
        return $this->id;
    }
}