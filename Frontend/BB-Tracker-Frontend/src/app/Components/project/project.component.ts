import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { NgRedux } from '@angular-redux/store';
import {MdDialog, MdDialogConfig} from '@angular/material';

import { IProjectStore } from "../../Store/store";
import { Observable } from 'rxjs/Observable';

import { TaskEditorComponent } from '../../Dialog/task-editor/task-editor.component';
import {IProject} from "../../Interfaces/project";
import {ProjectEditorComponent} from "../../Dialog/project-editor/project-editor.component";

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {
  project$: Observable<IProject>;
  projectId: number;
  constructor(private route: ActivatedRoute, private NgRedux: NgRedux<IProjectStore>, public dialog: MdDialog) {

  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.project$ = this.NgRedux.select<Object>(['projects', params['id']]);
      this.projectId = params['id'];
    });
  }

  public openTaskEditor(){
    let taskEditor = this.dialog.open(TaskEditorComponent, <MdDialogConfig>{data: {projectId: this.projectId}} );
  }

  public openProjectEditor(){
    this.dialog.open(ProjectEditorComponent, <MdDialogConfig>{data: {projectId: this.projectId}} )
  }

}
