import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from "@angular/router";
import {ITask} from "../../Interfaces/task";
import {NgRedux} from "@angular-redux/store";
import {IProjectStore} from "../../Store/store";
import {Observable} from "rxjs";

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {
  public task$: Observable<ITask>;
  public taskId: number;
  constructor(private route: ActivatedRoute, private NgRedux: NgRedux<IProjectStore>){
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.task$ = this.NgRedux.select<Object>(['projects', params['id'], 'tasks', params['taskId']]);
      this.taskId = params['id'];
    });
  }

}
