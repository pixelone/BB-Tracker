import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../Services/api.service';
import { MdDialog } from '@angular/material';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';


import { select } from '@angular-redux/store';
import { Observable } from 'rxjs/Observable';

import { ProjectEditorComponent } from '../../Dialog/project-editor/project-editor.component';
import {IProject} from "../../Interfaces/project";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @select() public readonly projects$: Observable<IProject[]>;

  constructor(private ApiService: ApiService, public dialog: MdDialog, public MdSnackBar: MdSnackBar) { }

  ngOnInit() {
    this.fetchProjects();
  }

  public fetchProjects(){
    //this.ApiService.getProjects();
  }

  public openProjectEditor(){
    let projectEditor = this.dialog.open(ProjectEditorComponent);
    projectEditor.afterClosed().subscribe(
        data => this.MdSnackBar.open('Project Successfully created', 'Ok', <MdSnackBarConfig>{duration: 3000} ),
        err => this.MdSnackBar.open(err, 'Shit')
    );
  }

}
