import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Rx';
import { Http, Response, Headers, URLSearchParams } from '@angular/http';
import { TrackerActions} from '../Store/app.actions';
import { NgRedux } from '@angular-redux/store';
import { IProjectStore } from "../Store/store";

const baseUrl = 'http://127.0.0.1';

@Injectable()
export class ApiService {

  constructor(private http: Http,
              private TrackerActions: TrackerActions,
              private ngRedux: NgRedux<IProjectStore>
  ) { }

  /**
   *
   * @param projectId
   * @returns {Observable<R>}
   */
  public getProjects(projectId ?: string){
    projectId = (projectId === undefined) ? '' : projectId;
    let Observable = this.http.get(`${baseUrl}/project/json/` + projectId )
        .map(this.extractData)
        .catch(this.handleError);
      Observable.subscribe(
        data => {
          for (let project of data){
            this.ngRedux.dispatch(this.TrackerActions.addProjects(project))
          }
        }
      );
    return Observable
  }

  public postProjects(data: {}){
    this.ngRedux.dispatch(this.TrackerActions.addProjects(data));
  }

  public putProjects(projectId: string, data: {}){
    projectId = (projectId === undefined) ? '' : projectId;
    return this.http.put(`${baseUrl}/project/json` + projectId, data)
        .map(this.extractData)
        .catch(this.handleError);
  }

  public getTasks(param?: {}){
    return this.http.get(`${baseUrl}/task/json`)
        .map(this.extractData)
        .catch(this.handleError);
  }

  public postTask(data){
    this.ngRedux.dispatch(this.TrackerActions.addTask(data));
  }



  private extractData(res: Response){
    let response = res.json();
    if(response.error) return this.handleError(res);
    return response.data || {};
  }

  private handleError(error: Response | any){
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.errorMessage || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}