import { Component, OnInit } from '@angular/core';
import { MdDialogRef } from '@angular/material';
import { ApiService } from '../../Services/api.service';
import {NgRedux} from "@angular-redux/store";
import {IProjectStore} from "../../Store/store";
import {IProject} from "../../Interfaces/project";

@Component({
  selector: 'app-project-editor',
  templateUrl: './project-editor.component.html',
  styleUrls: ['./project-editor.component.scss']
})
export class ProjectEditorComponent implements OnInit {
  public data: IProject;
  constructor(public dialogRef: MdDialogRef<ProjectEditorComponent>, private ApiService: ApiService, private NgRedux: NgRedux<IProjectStore>){

  }

  ngOnInit() {
    if("data" in this.dialogRef.config){
      this.NgRedux.select<IProject>(['projects', this.dialogRef.config.data.projectId]).subscribe(
          data => this.data = Object.assign({}, data)
      )
    } else {
      this.initState();
    }
  }

  public initState(){
    this.data = {
      tasks: []
    };
  }

  public save(){
    this.ApiService.postProjects(this.data);
    this.dialogRef.close(true);
    this.initState();
  }

}
