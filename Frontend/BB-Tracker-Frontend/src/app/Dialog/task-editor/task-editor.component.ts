import { Component, OnInit } from '@angular/core';
import { MdDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ApiService } from '../../Services/api.service';
import { ITask } from '../../Interfaces/task';

@Component({
  selector: 'app-task-editor',
  templateUrl: './task-editor.component.html',
  styleUrls: ['./task-editor.component.scss']
})
export class TaskEditorComponent implements OnInit {
  task: FormGroup;
  private projectId: number;
  private existingTask?: ITask;
  constructor(public dialogRef: MdDialogRef<TaskEditorComponent>, private formBuilder: FormBuilder, private ApiService: ApiService) {

  }

  ngOnInit() {
    this.task = this.buildForm();
    this.projectId = this.dialogRef.config.data.projectId;
  }

  public buildForm(){
    return this.formBuilder.group({
      title: ['', [Validators.required, Validators.minLength(2)]],
      description: [''],
    });
  }

  public save({value}: { value: ITask }){
    this.ApiService.postTask({task: value, projectId: this.projectId});
    this.dialogRef.close(true);
    this.buildForm();
  }


}
