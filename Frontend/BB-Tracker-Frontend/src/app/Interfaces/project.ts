import { ITask } from "./task";
/**
 * Project
 *
 * @copyright   Copyright (C) 2017 Fabien Robert. All rights reserved.
 * @author      Fabien Robert (fabien@pixelone.fr)
 */

export interface IProject {
    id?: number;
    title?: string;
    description?: string;
    tasks?: ITask[];
}

export class Project implements IProject{
    public id;
    public title;
    public description;
    public tasks;
}