/**
 * TASK Model
 * @copyright   Copyright (C) 2017 Fabien Robert. All rights reserved.
 * @author      Fabien Robert (fabien@pixelone.fr)
 */
export interface ITask {
    id: number;
    title: string;
    description: string;
}

export class Task implements ITask{
    public id;
    public title;
    public description;

    public static getForm(){

    }
}