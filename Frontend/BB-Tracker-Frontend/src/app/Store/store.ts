import { Action } from 'redux';
import { TrackerActions } from './app.actions';
import { Project } from '../Interfaces/project';

import { applyMiddleware, Store, createStore } from 'redux';

export interface ActionData extends Action{
    data ?: any,
}

export interface IProjectStore {
    projects: Project[];
}

export const INITIAL_STATE: IProjectStore = {
    projects: []
};

export const ProjectStore: Store<IProjectStore> = createStore(
    rootReducer,
    (localStorage.getItem('reduxState')) ? JSON.parse(localStorage.getItem('reduxState')) : INITIAL_STATE,
    applyMiddleware()
);

export function rootReducer(lastState: IProjectStore = INITIAL_STATE, action: ActionData): IProjectStore {
    switch(action.type) {
        case TrackerActions.ADD_PROJECT:
            let projects = lastState.projects;
            if(!('id' in action.data)){
                action.data.id = projects.length;
                projects.push(action.data);
            } else {
                projects[action.data.id] = action.data;
            }
            return Object.assign({}, lastState, {projects: projects});
        case TrackerActions.ADD_TASK:
            let previousProjectTree = lastState.projects;
            if(!('id' in action.data.task)) action.data.task.id = previousProjectTree[action.data.projectId].tasks.length;
            previousProjectTree[action.data.projectId].tasks.push(action.data.task);
            return Object.assign({}, lastState, {projects: previousProjectTree});
        default:
            return lastState;
    }
}