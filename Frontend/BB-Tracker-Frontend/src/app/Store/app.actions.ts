/**
 * Created by fro on 04/04/17.
 */
import { Injectable } from '@angular/core';
import { ActionData } from './store';
import { ITask } from "../Interfaces/task";

@Injectable()
export class TrackerActions {
    static ADD_PROJECT = 'ADD_PROJECT';
    static ADD_TASK = 'ADD_TASK';

    public addProjects(data: Object): ActionData {
        return { type: TrackerActions.ADD_PROJECT, data: data };
    }

    public addTask(data: ITask): ActionData {
        return { type: TrackerActions.ADD_TASK, data: data };
    }
}