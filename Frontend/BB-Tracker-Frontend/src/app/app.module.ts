import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { MaterialModule } from '@angular/material';
import 'hammerjs';
import { ReactiveFormsModule } from '@angular/forms';

//STORE
import { NgReduxModule, NgRedux, DevToolsExtension } from '@angular-redux/store';
import { IProjectStore, ProjectStore} from './Store/store';
import { TrackerActions } from './Store/app.actions';

import { AppComponent } from './app.component';
import { DashboardComponent } from './Components/dashboard/dashboard.component';
import { ApiService } from './Services/api.service';
import { ProjectEditorComponent } from './Dialog/project-editor/project-editor.component';
import { ProjectComponent } from './Components/project/project.component';
import { TaskComponent } from './Components/task/task.component';
import { TaskEditorComponent } from './Dialog/task-editor/task-editor.component';

const appRoutes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: 'project/:id', component: ProjectComponent},
  { path: 'project/:id/:taskId', component: TaskComponent},
  { path: '', redirectTo: '/dashboard', pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ProjectEditorComponent,
    ProjectComponent,
    TaskComponent,
    TaskEditorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    RouterModule.forRoot(appRoutes),
    NgReduxModule,
    ReactiveFormsModule
  ],
  entryComponents: [ProjectEditorComponent, TaskEditorComponent],
  providers: [ApiService, TrackerActions],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(ngRedux: NgRedux<IProjectStore>, devTools: DevToolsExtension) {
    ngRedux.provideStore(ProjectStore);
    ProjectStore.subscribe(()=>{
      localStorage.setItem('reduxState', JSON.stringify(ProjectStore.getState()))
    })
  }
}
