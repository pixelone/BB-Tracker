import { BBTrackerFrontendPage } from './app.po';

describe('bb-tracker-frontend App', () => {
  let page: BBTrackerFrontendPage;

  beforeEach(() => {
    page = new BBTrackerFrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
