module.exports = {
    directoryIndex: '/BB-Tracker/',
    navigateFallback: '/BB-Tracker/',
    stripPrefixMulti: {'dist/': 'BB-Tracker/'},
    root: 'dist/',
    staticFileGlobs: [
        'dist/index.html',
        'dist/**.js',
        'dist/**.css'
    ]
};